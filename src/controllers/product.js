const ProductService = require("../services/product");

exports.createProduct = async (req, res, next) => {
  const productService = new ProductService();
  const product = await productService.addProduct(req.body);
  req.io.emit("list:products", await productService.getAllProducts());
  res.redirect("/ingreso");
};

exports.listProducts = async (req, res, next) => {
  const productService = new ProductService();
  const products = await productService.getAllProducts();
  res.json(products);
};

exports.updateProduct = async (req, res, next) => {
  const productService = new ProductService();
  const {
    params: { id },
    body,
  } = req;
  const productUpdated = await productService.updateProduct(id, body);
  req.io.emit("list:products", await productService.getAllProducts());
  res.json({ msg: "product updated" });
};

exports.getProduct = async (req, res, next) => {
  const { id } = req.params;
  const product = await productService.getProduct(id);
  res.json(product);
};

exports.deleteProduct = async (req, res, next) => {
  const { id } = req.params;
  const productDeleted = await productService.deleteProduct(id);
  req.io.emit("list:products", await productService.getAllProducts());
  res.json({ msg: "product deleted" });
};
