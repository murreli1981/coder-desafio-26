const passport = require("passport");

exports.registerPage = async (req, res, next) => {
  res.render("register");
};

exports.loginPage = async (req, res, next) => {
  res.render("login");
};

exports.register = async (req, res, next) => {
  req.session.isLogged = true;
  res.render("input", { username: req.user.username });
};

exports.login = async (req, res, next) => {
  req.session.isLogged = true;
  res.render("input", { username: req.user.username });
};

exports.loginFailed = async (req, res, next) => {
  res.render("login-failed");
};

exports.registerFailed = async (req, res, next) => {
  res.render("register-failed");
};

exports.logout = async (req, res, next) => {
  req.logout();
  res.render("login");
};
