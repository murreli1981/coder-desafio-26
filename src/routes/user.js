const passport = require("passport");
const {
  loginPage,
  registerPage,
  login,
  register,
  loginFailed,
  registerFailed,
  logout,
} = require("../controllers/user");

module.exports = (router) => {
  router.get("/login", loginPage);
  router.post(
    "/login",
    passport.authenticate("login-local", { failureRedirect: "/login-failed" }),
    login
  );
  router.get("/register", registerPage);
  router.post(
    "/register",
    passport.authenticate("register-local", {
      failureRedirect: "/register-failed",
    }),
    register
  );
  router.get("/login-failed", loginFailed);
  router.get("/register-failed", registerFailed);
  router.get("/logout", logout);

  return router;
};
