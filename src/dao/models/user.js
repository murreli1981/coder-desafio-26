const { Schema, model } = require("mongoose");

const productSchema = new Schema({
  username: {
    unique: true,
    type: String,
  },
  hash: String,
});

module.exports = model("User", productSchema);
