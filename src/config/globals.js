require("dotenv").config();

module.exports = {
  PORT: process.env.PORT || 8080,
  MONGO_URI: process.env.MONGO_URI || "undefined",
  SECRET_KEY: process.env.SECRET_KEY || "qwertyuiop",
};
